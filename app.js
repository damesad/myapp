
var app = angular.module("myappprueva",['ngRoute','firebase']);

app.config(["$routeProvider",function($routeProvider){
     $routeProvider.when("/",{
          controller:"homeCtrl",
          templateUrl:"vistas/home.html"
     });
}]);

app.controller("homeCtrl",function($scope,$firebaseAuth){
    
     var ref = new Firebase("https://myappprueva.firebaseio.com/");
     // create an instance of the authentication service
     var auth = $firebaseAuth(ref);
     // login with Facebook
     
     $scope.login = function(){

     auth.$authWithOAuthPopup("github").then(function(authData){ 
              console.log("ok");
          },
          {
               scope: "user,gist"
          });
     }  
});